#include <systemc.h>
#include <iostream>
#include "computer.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {

  cout << "SystemC main started" << endl;

  Computer computer("computer");

  cout << "Computer instance created" << endl;

  sc_start();

  cout << "Simulation ended" << endl;


  return 0;
}
