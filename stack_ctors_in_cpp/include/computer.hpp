#ifndef COMPUTER_HPP
#define COMPUTER_HPP

#include "cpu.hpp"
#include "memory.hpp"

SC_MODULE(Computer){
	Cpu cpu;
	Memory memory;
	Computer(sc_module_name name);	
};
	

#endif
