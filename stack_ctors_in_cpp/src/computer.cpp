#include <systemc.h>
#include <iostream>
#include "computer.hpp"


SC_HAS_PROCESS(Computer);

Computer::Computer(sc_module_name name) :
    sc_module(name), memory("memory"), cpu("cpu") {
		
		std::cout << "Computer constructor called" << std::endl;
		
	}

