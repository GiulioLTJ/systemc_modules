#ifndef COMPUTER_HPP
#define COMPUTER_HPP
#include "memory.hpp"
#include "cpu.hpp"

SC_MODULE(Computer){
	Cpu cpu;
	Memory memory;
	SC_CTOR(Computer) : memory("memory"), cpu("cpu")  {
		   std::cout << "Computer constructor called" << std::endl;
	   }
};
	

#endif
