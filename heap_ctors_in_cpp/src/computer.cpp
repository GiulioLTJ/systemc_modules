#include <systemc.h>
#include <iostream>
#include "computer.hpp"
#include "memory.hpp"
#include "cpu.hpp"

SC_HAS_PROCESS(Computer);

Computer::Computer(sc_module_name name) :
    sc_module(name), memory(new Memory("memory")), cpu(new Cpu("cpu")) {
		
		std::cout << "Computer constructor called" << std::endl;
		
	}
Computer::~Computer() {
	std::cout << "Computer destructor called" << std::endl;
	delete memory;
	delete cpu;
}
