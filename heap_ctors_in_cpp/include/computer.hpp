#ifndef COMPUTER_HPP
#define COMPUTER_HPP

class Cpu;
class Memory;

SC_MODULE(Computer){
	Cpu* cpu;
	Memory* memory;
	Computer(sc_module_name name);
	~Computer();
};
	

#endif
