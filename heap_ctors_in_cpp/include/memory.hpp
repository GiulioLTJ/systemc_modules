#ifndef MEMORY_HPP
#define MEMORY_HPP

#include <iostream>

SC_MODULE(Memory){
	SC_CTOR(Memory) {std::cout<<"Memory constructor called" << std::endl;}
	~Memory() { std::cout << "Memory destructor called" << std::endl; }
};

#endif
