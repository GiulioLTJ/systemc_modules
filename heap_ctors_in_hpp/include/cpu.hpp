#ifndef CPU_HPP
#define CPU_HPP

#include <iostream>

SC_MODULE(Cpu){
	SC_CTOR(Cpu) {std::cout<<"Cpu constructor called" << std::endl;}
	~Cpu() { std::cout << "Cpu destructor called" << std::endl; }
};

#endif
