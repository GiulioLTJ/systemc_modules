#ifndef COMPUTER_HPP
#define COMPUTER_HPP
#include "memory.hpp"
#include "cpu.hpp"

SC_MODULE(Computer){
	Cpu* cpu;
	Memory* memory;
	SC_CTOR(Computer) : 
	   memory(new Memory("memory")), cpu(new Cpu("cpu"))  {
		   std::cout << "Computer constructor called" << std::endl;
	   }
	
	~Computer()  {
		std::cout << "Computer destructor called" << std::endl;
		delete cpu;
		delete memory;
	}
};
	

#endif
